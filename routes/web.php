<?php

/** @var \Laravel\Lumen\Routing\Router $router */


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
 * --------------------------------------------------------------
 * API ROUTE
 * --------------------------------------------------------------
 */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login', 'AuthController@authenticate');
    $router->post('register', 'AuthController@register');
});

$router->group(["prefix" => "games", 'middleware' => 'jwt.auth'], function () use ($router) {

    /*
        * --------------------------------------------------------------
        * Get - Overview
        * --------------------------------------------------------------
        *
        * This Route will return all the games saved.
        *
        */
    $router->get('/', 'GameController@getGames');

    /*
     * --------------------------------------------------------------
     * POST - Start new SuggestedGame
     * --------------------------------------------------------------
     *
     * This Route will generate a new game and save it in session.
     *
     */
    $router->post('new', 'GameController@newGame');
    /*
     * --------------------------------------------------------------
     * GET - JSON Response
     * --------------------------------------------------------------
     *
     * This Route will return the status try. If the player
     * won or be hanged, the response will alert.
     *
     */
    $router->get('status/{id}', 'GameController@getTry');

    /*
     * --------------------------------------------------------------
     * POST - Guessing the Word
     * --------------------------------------------------------------
     *
     * This Route will get the $id to find which word the user is
     * trying to guess.
     *
     */
    $router->post('guess', 'GameController@guessingWord');

    /*
     * --------------------------------------------------------------
     * GET - Reset SuggestedGame
     * --------------------------------------------------------------
     *
     * This Route will reset a saved SuggestedGame to the default values.
     *
     */
    $router->get('reset/{$id}', 'GameController@reset');
    /*
     * --------------------------------------------------------------
     * GET - Delete SuggestedGame saved
     * --------------------------------------------------------------
     *
     * This Route will delete a saved SuggestedGame.
     *
     */
    $router->delete('delete/{id}', 'GameController@delete');

    $router->get("/delete/all", 'GameController@deleteAll');

});
