<?php namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * Class GameConfigurations
 *
 * @package Hangman
 */
class GameConfigurations
{
    private $sessionName;
    private $id;
    private $word;

    /**
     * Start the session name
     * @param id - user id
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->sessionName = trim("guess_game" . $id);
    }

    /**
     * @return string
     */
    public function getSessionName()
    {
        return $this->sessionName;
    }

    /**
     * Create a new SuggestedGame
     *
     * @param int $id
     * @return SuggestedGame
     */
    public function newGame($id)
    {
        $suggestedGame = new SuggestedGame();
        $suggestedGame->make($id);
    }

    /**
     * @param Word $word
     */
    public function initWord(Word $word)
    {
        $this->word = $word;
    }

    /**
     * Check if exists a SuggestedGame saved then load it, otherwise, return null
     *
     * @return array|SuggestedGame|null
     */
    public function load()
    {
        if (Cache::has($this->sessionName)) {
            $session = Cache::get($this->sessionName);

            if (!isset($session["id"])) {
                $game = array();

                foreach ($session as $key => $val) {
                    $suggestedGame = new SuggestedGame();
                    $game[] = $suggestedGame->make($val["id"], $val["tries_left"], $val["tried_letters"], $val["found_letters"], $val["found_string"], $val["status"], $val["found_letters_helper"]);
                }
                Log::debug('load games: ' . $game);
                return $game;
            } else {
                $game = new SuggestedGame();
                $game->make(
                    $session["id"],
                    $session["tries_left"],
                    $session["tried_letters"],
                    $session["found_letters"],
                    $session["found_string"],
                    $session["status"],
                    $session["found_letters_helper"]
                );
                $game->setWordByIndex((int)$session['wordIndex']);
                return $game;
            }
        }
        Log::debug('is null');
        return null;
    }

    /**
     * Save the SuggestedGame in session
     * @param SuggestedGame $game - SuggestedGame object
     */
    public function save(SuggestedGame $game)
    {
        Cache::put($this->sessionName, $game->getCurrent($save = true));
    }

    /**
     * Reset this session SuggestedGame
     */
    public function reset()
    {
        $this->delete();
        $game = new SuggestedGame();
        $game->make($this->id);
        Cache::put($this->sessionName, $game->getCurrent());
    }

    /**
     * Delete this session SuggestedGame
     */
    public function delete()
    {
        Cache::forget($this->sessionName);
    }

    /**
     * Delete all Games saved
     */
    public function deleteAll()
    {
        Cache::flush();
    }

    /**
     * Generate the session name to add and array of session
     * to multiple games
     *
     * @param $id
     * @return $this
     */
    public function generateSessionNameWithIndex($id)
    {
        $this->id = $id;
        $this->sessionName = trim($this->sessionName . $id);
        return $this;
    }
}
