<?php


namespace App\Models;


abstract class AbstractFactoryMethod
{
    abstract function make($params);
}
