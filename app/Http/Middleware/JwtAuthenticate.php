<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Log;

class JwtAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->token;
        if (!$token) {
            //Unauthorized
            return response()->json(['error' => 'token not provided'], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'),['HS256']);
        } catch (ExpiredException $expiredException) {
            return response()->json(['error' => $expiredException->getMessage()], 401);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 400);
        }
        $user = User::find($credentials->sub);
        $request->auth = $user;
        return $next($request);
    }
}
