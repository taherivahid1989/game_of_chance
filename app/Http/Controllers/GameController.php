<?php

namespace App\Http\Controllers;

use App\Models\GameConfigurations;
use App\Models\User;
use App\Models\Word;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getGames(Request $request)
    {
        try {
            $user = $request->auth;
            $configuration = new GameConfigurations($user->id);
            /**
             * Load  the games saved
             */
            $game = $configuration->load();

            return response()->json($game->apiResponse());
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 400);
        }
    }

    public function newGame(Request $request)
    {
        try {
            $word = new Word();
            $user = $request->auth;
            $configuration = new GameConfigurations($user->id);
            /**
             * Create a new SuggestedGame with the index of the word chosen randomly.
             */
            $game = $configuration->newGame($user->id);
            /**
             * Set the word in the SuggestedGame for some basic configurations
             */
            $game->setWord($word);

            /**
             * Save the SuggestedGame
             */
            $configuration->save($game);

            return response()->json($game->apiResponse());
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function getTry(Request $request, $id)
    {
        try {
            $user = $request->auth;
            $word = new Word();
            $configuration = new GameConfigurations($user->id);
            $game = $configuration->load();

            /**
             * Check if the game has been loaded correctly or if the player
             * is not trying to access this ROUTE before generate a new SuggestedGame
             */
            if (is_null($game)) {
                return response()->json(
                    ["message" => "SuggestedGame not found"]
                    , 400);
            }

            /**
             * Check if the player has won
             */
            if ($game->hasWon()) {
                return response()->json(
                    ["status" => 200, "success_code" => "HAN04", "message" => "Player won the game."]
                );
            } /**
             * Check if the player was hanged
             */
            else if ($game->hasHanged()) {

                return response()->json(
                    ["message" => "Player hanged.", "correct_word" => $game->getWord()
                    ]);
            }
            return response()->json($game->apiResponse());

        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function guessingWord(Request $request)
    {
        try {
            $this->validate($request, [
                'char' => 'required|regex:/^[a-zA-Z]+$/u'
            ]);
            $op = $request->char;
            /**
             * Check if the value that came from POST are not null, empty
             * or even a string
             */
            if (is_null($op) || empty($op) || !is_string($op)) {
                return response()->json(
                    ["status" => 400, "message" => "Parameter missing."]
                );
            }
            $user = $request->auth;
            $configuration = new GameConfigurations($user->id);
            $game = $configuration->load();

            /**
             * Check if the game has been loaded correctly or if the player
             * is not trying to access this ROUTE before generate a new SuggestedGame
             */
            if (is_null($game)) {
                return response()->json(
                    ["message" => "SuggestedGame not found"]
                    , 400);
            }
            /**
             * If the player has tries left, the API will check the letter,
             * otherwise, the response will be negative.
             */
            if ($game->getTriesLeft() > 0) {
                /**
                 * Check the letter that the player guessed
                 */
                $checkLetterReturn = $game->checkLetter($op);

                if (!is_bool($checkLetterReturn)) {
                    return response()->json(
                        ["status" => 400, "message" => $checkLetterReturn]);
                }

                /**
                 * Save the new result in session
                 */
                $configuration->save($game);
            } else {
                return response()->json(
                    ["status" => 400, "error_code" => "HAN03", "message" => "Exceeded attempts"]
                );
            }


            return response()->json(["status" => 200]);

        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function reset()
    {
        try {
            $configuration = new GameConfigurations();

            /**
             * Load the game saved
             */
            $game = $configuration->generateSessionNameWithIndex($id)->load();

            /**
             * Check if the game has been loaded correctly or if the player
             * is not trying to access this ROUTE before generate a new SuggestedGame
             */
            if (is_null($game)) {
                return response()->json(["status" => 400, "error_code" => "HAN01", "message" => "SuggestedGame not found"]);
            }

            /**
             * Reset the SuggestedGame to the default values
             */
            $configuration->reset();

            return response()->json(["status" => 200, "success_code" => "HAN07", "message" => "SuggestedGame was reset."]);

        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function delete($id)
    {
        try {

            $configuration = new GameConfigurations();

            /**
             * Load the game saved
             */
            $game = $configuration->generateSessionNameWithIndex($id)->load();

            /**
             * Check if the game has been loaded correctly or if the player
             * is not trying to access this ROUTE before generate a new SuggestedGame
             */
            if (is_null($game)) {
                return response()->json(["status" => 400, "error_code" => "HAN01", "message" => "SuggestedGame not found"]);
            }

            /**
             * Reset the SuggestedGame to the default values
             */
            $configuration->delete();

            return response()->json(["status" => 200, "success_code" => "HAN07", "message" => "SuggestedGame was reset."]);

        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    public function deleteAll()
    {
        try {
            $configuration = new GameConfigurations();
            $configuration->deleteAll();
            return response()->json(["status" => 200, "success_code" => "HAN07", "message" => "All game was delete."]);

        } catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }
}
